/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page SCT_ConditionsData_page SCT_ConditionsData package
@author Shaun.Roe@cern.ch, Jorgen.Dalmau@cern.ch


@section SCT_ConditionsData_SCT_ConditionsDataIntro Introduction
This package provides data classes used to access data in the conditions
 database or derived from the datastream in some way. Access to these underlying data classes is
 provided by the AlgTools in SCT_ConditionsAlgs, and more documentation can be found there.
  - SCT DAQ calibration data
  - SCT DCS conditions



@section SCT_ConditionsData_SCT_ConditionsDataOverview Class overview
 
  - SCT_CalibDefectData: Container with a list of defects derived from calibration data
  
  - StringifyVector: Local utility function to tokenise and stream a string to a vector.

@section SCT_ConditionsData_SCT_ConditionsDataRefs Reference pages
*/
