#include "TrigEgammaHypo/TrigL2CaloHypo.h"
#include "TrigEgammaHypo/TrigL2ElectronFex.h"
#include "TrigEgammaHypo/TrigL2PhotonFex.h"
#include "TrigEgammaHypo/TrigL2ElectronHypo.h"
#include "../TrigL2PhotonFexMT.h"
#include "TrigEgammaHypo/TrigL2PhotonHypo.h"
#include "TrigEgammaHypo/TrigEFDielectronMassHypo.h"
#include "TrigEgammaHypo/TrigEFDielectronMassFex.h"
#include "TrigEgammaHypo/TrigEFMtAllTE.h"
#include "TrigEgammaHypo/TrigEFCaloCalibFex.h"
#include "TrigEgammaHypo/TrigEFCaloHypo.h"
#include "TrigEgammaHypo/TrigEFElectronHypo.h"
#include "TrigEgammaHypo/TrigEFPhotonHypo.h"
#include "TrigEgammaHypo/TrigEFTrackHypo.h"
#include "TrigEgammaHypo/TrigL2CaloLayersHypo.h"
#include "../TrigL2CaloHypoAlgMT.h"
#include "../TrigL2CaloHypoToolInc.h"
#include "../TrigEgammaPrecisionCaloHypoToolInc.h"
#include "../TrigEgammaPrecisionPhotonHypoToolInc.h"
#include "../TrigL2CaloHypoToolMult.h"
#include "../TrigL2ElectronHypoTool.h"
#include "../TrigL2ElectronFexMT.h"
#include "../TrigL2ElectronHypoAlgMT.h"
#include "../TrigL2PhotonHypoAlgMT.h"
#include "../TrigL2PhotonHypoTool.h"
#include "../TrigEgammaPrecisionCaloHypoAlgMT.h"
#include "../TrigEgammaPrecisionCaloHypoToolMult.h"
#include "../TrigEgammaPrecisionPhotonHypoAlgMT.h"
#include "../TrigEgammaPrecisionPhotonHypoToolMult.h"

DECLARE_COMPONENT( TrigL2CaloHypo )
DECLARE_COMPONENT( TrigL2ElectronFex )
DECLARE_COMPONENT( TrigL2PhotonFex )
DECLARE_COMPONENT( TrigL2ElectronHypo )
DECLARE_COMPONENT( TrigL2PhotonFexMT )
DECLARE_COMPONENT( TrigL2PhotonHypo )
DECLARE_COMPONENT( TrigEFDielectronMassHypo )
DECLARE_COMPONENT( TrigEFDielectronMassFex )
DECLARE_COMPONENT( TrigEFMtAllTE )
DECLARE_COMPONENT( TrigEFCaloCalibFex )
DECLARE_COMPONENT( TrigEFCaloHypo )
DECLARE_COMPONENT( TrigEFElectronHypo )
DECLARE_COMPONENT( TrigEFPhotonHypo )
DECLARE_COMPONENT( TrigEFTrackHypo )
DECLARE_COMPONENT( TrigL2CaloLayersHypo )
DECLARE_COMPONENT( TrigL2CaloHypoAlgMT )
DECLARE_COMPONENT( TrigL2ElectronHypoAlgMT )
DECLARE_COMPONENT( TrigL2PhotonHypoAlgMT )
DECLARE_COMPONENT( TrigL2CaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoToolInc )
DECLARE_COMPONENT( TrigL2CaloHypoToolMult )
DECLARE_COMPONENT( TrigL2ElectronHypoTool )
DECLARE_COMPONENT( TrigL2ElectronFexMT )
DECLARE_COMPONENT( TrigL2PhotonHypoTool )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoToolMult )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoToolMult )
